﻿#pragma warning disable 0649

using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MenuScript : MonoBehaviour
{

    private string isSoundOn;
    private int isInfinityEnable;

    [SerializeField] private Sprite[] spriteMassive;
    public UnityEngine.UI.Button Campaign;
    public UnityEngine.UI.Text InfinityScore;
    public GameObject Infinity;
    public GameObject Sound;
    public GameObject Message;
    public GameObject CampMessage;

    private void Start()
    {

        InfinityScore.text = PlayerPrefs.GetInt("InfinityScore").ToString();
        isSoundOn = PlayerPrefs.GetString("Sound");
        PlayerPrefs.SetInt("Infinity", 1);
        if(isSoundOn == "1")
        {
            AudioListener.pause = false;
            Sound.GetComponent<Image>().sprite = spriteMassive[1];
        }
        else
        {
            AudioListener.pause = true;
            Sound.GetComponent<Image>().sprite = spriteMassive[0];
        }


        isInfinityEnable = PlayerPrefs.GetInt("Infinity");
        if (isInfinityEnable == 0) Infinity.SetActive(false);
    }
    
    public void OnInfinity()
    {
        if(isInfinityEnable == 1)
            SceneManager.LoadScene(1);
        else
        {
            Message.SetActive(true);
        }
    }

    public void OnCampaign()
    {
        
        
            CampMessage.SetActive(true);
        
    }

    public void OnSound()
    {
        if (isSoundOn == "0")
        {
            AudioListener.pause = false;
            isSoundOn = "1";
            PlayerPrefs.SetString("Sound", "1");
            Sound.GetComponent<Image>().sprite = spriteMassive[1];

        }
        else
        {
            AudioListener.pause = true;
            isSoundOn = "0";
            PlayerPrefs.SetString("Sound", "0");
            Sound.GetComponent<Image>().sprite = spriteMassive[0];
        }
        
    }
    
    public void OnExit()
    {
        Application.Quit();
    }
    
    public void OnCloseMessage()
    {
        Message.SetActive(false);
    }
    public void OnCloseCampMessage()
    {
        CampMessage.SetActive(false);
    }
}
