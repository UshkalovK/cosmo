﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePartsScript : MonoBehaviour
{
    public float minSpeed;
    public float maxSpeed;
    void Start()
    {
        Rigidbody part = GetComponent<Rigidbody>();
        part.DOMoveZ(-100, Random.Range(minSpeed, maxSpeed));
    }
}
