﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour
{
    public float speed;
	public bool checkEnemy;
    void Start()
    {
		if(checkEnemy) speed = -speed;
        this.GetComponent<Rigidbody>().DOMoveZ(speed, 5);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
