﻿#pragma warning disable 0649

using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidScript : MonoBehaviour
{
    [SerializeField] GameObject asteroidExp;
    [SerializeField] GameObject playerExp;
    [SerializeField] float rotationSpeed;
    [SerializeField] float minSpeed;

    public int score;
    public float maxSpeed;

    public static AsteroidScript instance;
    void Start()
    {
        Rigidbody asteroid = GetComponent<Rigidbody>();
        asteroid.DORotate(new Vector3(0,840,0), 10f, RotateMode.FastBeyond360);
        asteroid.velocity = new Vector3(0, 0, -Random.Range(minSpeed, maxSpeed));
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Asteroid" || other.tag == "GameBoundary" || other.tag == "Enemy" || other.tag == "Heart")
        {
            return;
        }
        Instantiate(asteroidExp, transform.position, Quaternion.identity);
        if (other.tag == "Player")
        {
			Destroy(gameObject);
			return;
        }
		GameController.instance.score += 1;
        Destroy(gameObject);
        Destroy(other.gameObject);
        
    }

 

}
