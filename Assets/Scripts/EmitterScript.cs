﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmitterScript : MonoBehaviour
{
    public GameObject asteroid;
	public GameObject enemy;
    public GameObject heart;
    public float minDelay, maxDelay;
    private float nextLaunchTime = 5;  
	private float nextEnemyLaunchTime = 5;  
    private float nextHeartLaunchTime = 35;
    private float asteroidWeight;

    void Start()
    {
        Application.targetFrameRate = 60;
    }

    void FixedUpdate()
    {
        if(!PlayerScript.instance.isDead){
            if (Time.timeSinceLevelLoad > nextLaunchTime)
            {
                float emitterSize = transform.localScale.x;
                GameObject newAsteroid = Instantiate(asteroid, new Vector3(Random.Range(-emitterSize / 2, emitterSize / 2), 0, transform.position.z), Quaternion.identity);
                asteroidWeight = Random.Range(0.5f, 2);
                newAsteroid.transform.localScale *= asteroidWeight;
                nextLaunchTime = Time.timeSinceLevelLoad + Random.Range(minDelay, maxDelay);
            }
            if (Time.timeSinceLevelLoad > nextEnemyLaunchTime)
            {
                float emitterSize = transform.localScale.x;
                Instantiate(enemy, new Vector3(Random.Range(-emitterSize / 2, emitterSize / 2), 0, transform.position.z), Quaternion.identity);
                nextEnemyLaunchTime = Time.timeSinceLevelLoad + Random.Range(minDelay * 3, maxDelay * 3);
            }
            if (Time.timeSinceLevelLoad > nextHeartLaunchTime)
            {
                float emitterSize = transform.localScale.x;
                Instantiate(heart, new Vector3(Random.Range(-emitterSize / 2, emitterSize / 2), 0, transform.position.z), Quaternion.Euler(90, 0, 0));
                nextHeartLaunchTime = Time.timeSinceLevelLoad + Random.Range(minDelay * 20, maxDelay * 20);
            }
        }
    }
}
