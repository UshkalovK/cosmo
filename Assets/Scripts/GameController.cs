﻿#pragma warning disable 0649

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public UnityEngine.UI.Text scoreLabel;
    public GameObject healthLevel;

    public GameObject gameOverPanel;
    public GameObject PausePanel;
    public GameObject PauseButton;

    [SerializeField] private Sprite[] spriteMassive;
    public GameObject Sound;

    private int isSound;

    public int score = 0;
    
    public static GameController instance;
    void Start()
    {
        Debug.Log(Time.timeSinceLevelLoad);
        instance = this;
        isSound = PlayerPrefs.GetInt("Sound");

        
    }
    
    void Update()
    {
        scoreLabel.text =  score.ToString();
        if (PlayerScript.instance.health == 10) healthLevel.transform.localScale = new Vector3(1,1,1);
        if (PlayerScript.instance.health == 9) healthLevel.transform.localScale = new Vector3(0.9f, 1, 1);
        if (PlayerScript.instance.health == 8) healthLevel.transform.localScale = new Vector3(0.8f, 1, 1);
        if (PlayerScript.instance.health == 7) healthLevel.transform.localScale = new Vector3(0.7f, 1, 1);
        if (PlayerScript.instance.health == 6) healthLevel.transform.localScale = new Vector3(0.6f, 1, 1);
        if (PlayerScript.instance.health == 5) healthLevel.transform.localScale = new Vector3(0.5f, 1, 1);
        if (PlayerScript.instance.health == 4) healthLevel.transform.localScale = new Vector3(0.4f, 1, 1);
        if (PlayerScript.instance.health == 3) healthLevel.transform.localScale = new Vector3(0.3f, 1, 1);
        if (PlayerScript.instance.health == 2) healthLevel.transform.localScale = new Vector3(0.2f, 1, 1);
        if (PlayerScript.instance.health == 1) healthLevel.transform.localScale = new Vector3(0.1f, 1, 1);
        if (PlayerScript.instance.health == 0) healthLevel.transform.localScale = new Vector3(0, 1, 1);
    }

    public void OnPause()
    {
        Time.timeScale = 0;
        PausePanel.SetActive(true);
        if (PlayerPrefs.GetInt("Sound") == 1) Sound.GetComponent<Image>().sprite = spriteMassive[0];
        if (PlayerPrefs.GetInt("Sound") == 0) Sound.GetComponent<Image>().sprite = spriteMassive[1];
    }

    public void OnPlay()
    {
        Time.timeScale = 1;
        PausePanel.SetActive(false);
    }

    public void OnReset()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    }

    public void OnMenu()
    {
       SceneManager.LoadScene(0);
        Time.timeScale = 1;
    }


    public void OnSound()
    {
        if (PlayerPrefs.GetInt("Sound") == 0)
        {
            AudioListener.pause = false;
            PlayerPrefs.SetInt("Sound", 1);
            isSound = 1;
            Sound.GetComponent<Image>().sprite = spriteMassive[0];

        }
        else
        {
            AudioListener.pause = true;
            PlayerPrefs.SetInt("Sound", 0);
            isSound = 0;
            Sound.GetComponent<Image>().sprite = spriteMassive[1];
        }

    }

}
