﻿#pragma warning disable 0649

using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    [SerializeField] GameObject defaultState;
    [SerializeField] GameObject secondState;
    [SerializeField] GameObject thirdState;
    [SerializeField] GameObject fourthState;

    public float xMin, xMax, zMin, zMax;
    public float speed;
    public float tilt;
    public int health;
    public bool isDead = false;
    private bool isPowerUp = false;
    private Rigidbody Ship;
    public GameObject laserShot;
    public GameObject laserGun;
    public GameObject playerExp;
    public float shotDelay;
    float nextShotTime = 0;
    public static PlayerScript instance;

    void Start()
    {
        Ship = GetComponent<Rigidbody>();
        instance = this;
        health = 10;
        Input.acceleration.Normalize();
    }
    
    void Update()
    {
        #if UNITY_ANDROID
        Vector3 dir = Vector3.zero;
        dir.x = Input.acceleration.x;
        //dir.z = Input.acceleration.y;

        //clamp acceleration vector to unit sphere
        if (dir.sqrMagnitude > 1)
            dir.Normalize();

        // Make it move 10 meters per second instead of 10 meters per frame...
        Ship.velocity = dir * speed;
        Ship.DORotate(new Vector3(0,0,dir.x * tilt * 25), 30/speed);
        #else
        float moveX = Input.GetAxis("Horizontal");
        float moveZ = Input.GetAxis("Vertical");
        
        Ship.velocity = new Vector3(moveX, 0, moveZ) * speed;
        Ship.DORotate(new Vector3(0,0,-moveX * tilt * 25), 30/speed);
        #endif
        
        float clampedX = Mathf.Clamp(Ship.position.x, xMin, xMax);
        float clampedZ = Mathf.Clamp(Ship.position.z, zMin, zMax);

        Ship.position = new Vector3(clampedX, 0, clampedZ);

        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            speed += 30f;
            isPowerUp = true;
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            speed -= 30f;
            isPowerUp = false;
        }

        if (Time.time > nextShotTime && Input.GetButton("Fire1"))
        {
            Instantiate(laserShot, laserGun.transform.position, Quaternion.identity);
            nextShotTime = Time.time + shotDelay;
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Heart"){
            Destroy(other.gameObject);
            health = 10;
            speed = 150;
            if (isPowerUp) speed += 30;
            shotDelay = 0.3f;
            defaultState.SetActive(true);
            secondState.SetActive(false);
            thirdState.SetActive(false);
            fourthState.SetActive(false);       
        }
        
        if (other.tag == "Enemy" || other.tag == "Asteroid")
        {
            health -=1;
            Destroy(other.gameObject);
        
        if (health == 8)
        {
            defaultState.SetActive(false);
            secondState.SetActive(true);
        }
        if (health == 6)
        {
            secondState.SetActive(false);
            thirdState.SetActive(true);
            speed -= 10f;
            shotDelay +=0.3f;

        }
        if (health == 2)
        {
            thirdState.SetActive(false);
            fourthState.SetActive(true);
            speed -= 6f;
            shotDelay +=0.3f;
        }

            if (health <= 0)
            {
                Instantiate(playerExp, transform.position, Quaternion.identity);
                Destroy(gameObject);
                GameController.instance.gameOverPanel.SetActive(true);
                GameController.instance.PauseButton.SetActive(false);
                isDead = true;
                PlayerPrefs.SetInt("InfinityScore", PlayerPrefs.GetInt("InfinityScore")+GameController.instance.score);

            }
        }
    }
}
